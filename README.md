## Telegram echo bot in Docker container

### Goal

Running a simple telegram echo bot which sending back the user's messages.

The bot is running in docker container.

Learning how to prepare Dockerfile, build an image, running a Python script in container.

### Files

Echobot.py - Python 3.10.6 script, with simple telegram echo bot.

Dockerfile containing requirements and execution commands.

### Environment

Was tested on OS Ubuntu 22.04, terminal.

Docker version 20.10.12, build 20.10.12-0ubuntu4

Docker image is based on python 3.10.7-slim-buster + package python-telegram-bot 

### Running instruction

Before running docker image build insert inside bot script "echobot.py" valid TOKEN (replace `TOKEN` on string 33)

1. Open a new terminal window `CTRL + ALT + T`
2. Run command: 
```
docker image build [path to the directory containing Dockerfile & echobot.py script]
```
3. Run command: 
```
docker run [docker image ID]
```
4. Find bot through search in Telegram app, type `/start`, try the bot.

### Credits

Telegram echo bot source code was taken [here](https://github.com/python-telegram-bot/python-telegram-bot/blob/master/examples/echobot.py)
