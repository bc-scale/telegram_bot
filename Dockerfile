# syntax=docker/dockerfile:1

FROM python:3.10.7-slim-buster

RUN mkdir -p /app

RUN pip install python-telegram-bot

WORKDIR /app

COPY . .

CMD [ "python3", "echobot.py"]
